# What would I improve if I had more time

## `siren_evaluation` and `vat_evaluation`

I would reflect on this two functions.

They are really similar and need to be refactored. The refactor would depend on how
we want to handle evaluations.

- Will we have any other `evaluation type` in the futur ? And if so, will they only change from the previous
ones by how much we decrease the score in certain case ?

If the answer for that question is `yes` then we could merge the two function into one and create an `Hash` who would
look like this :
```ruby
{
  siren: {
    unconfirmed: 5,
    favorable: 1
  },
  vat: {
    unconfirmed: 1,
    favorable: 3
  }
}
```

I any case both of this functions still have to many `if`, `else` that tend to be confusion when used in profusion.
We can probably clean that a bit.

## `ongoing_database_update`

Can we have an evaluation with the reason `ongoing_database_update` and have a state different from `unconfirmed` ?
If no then there is no need to check the state on the first `if` in `siren_evaluation` and `vat_evaluation`.
Same for `unable_to_reach_api`.

## Validations

We should make sure the evaluations we receive are correct.

## Specs

We should had more specs to test every new function I made indivually, even if they
are all used in the already tested function `update_score()`.
