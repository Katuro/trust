require "json"
require "net/http"

class TrustIn
  OPENDATASOFT_URL = 'https://public.opendatasoft.com/api/records/1.0/search/?dataset=sirene_v3'
  MAX_SCORE = 100

  def initialize(evaluations)
    @evaluations = evaluations
  end

  def update_score()
    @evaluations.each do |evaluation|
      case evaluation.type
      when "SIREN"
        siren_evaluation(evaluation)
      when "VAT"
        vat_evaluation(evaluation)
      else
        raise 'error: Invalid evaluation type.'
      end
    end
  end

  def siren_evaluation(evaluation)
    if evaluation.score > 0 && evaluation.state == "unconfirmed" && evaluation.reason == "ongoing_database_update"
      set_max_score(evaluation)
    elsif evaluation.score >= 50
      if evaluation.state == "unconfirmed" && evaluation.reason == "unable_to_reach_api"
        evaluation.score = evaluation.score - 5
      elsif evaluation.state == "favorable"
        evaluation.score = evaluation.score - 1
      end
    elsif evaluation.score <= 50 && evaluation.score > 0
      if evaluation.state == "unconfirmed" && evaluation.reason == "unable_to_reach_api" || evaluation.state == "favorable"
        evaluation.score = evaluation.score - 1
      end
    else
      if evaluation.state == "favorable" || evaluation.state == "unconfirmed"
        set_max_score(evaluation)
      end
    end
  end

  def vat_evaluation(evaluation)
    if evaluation.score > 0 && evaluation.state == "unconfirmed" && evaluation.reason == "ongoing_database_update"
      set_max_score(evaluation)
    elsif evaluation.score >= 50
      if evaluation.state == "unconfirmed" && evaluation.reason == "unable_to_reach_api"
        evaluation.score = evaluation.score - 1
      elsif evaluation.state == "favorable"
        evaluation.score = evaluation.score - 3
      end
    elsif evaluation.score <= 50 && evaluation.score > 0
      if evaluation.state == "unconfirmed" && evaluation.reason == "unable_to_reach_api" || evaluation.state == "favorable"
        evaluation.score = evaluation.score - 3
      end
    else
      if evaluation.state == "favorable" || evaluation.state == "unconfirmed"
        set_max_score(evaluation)
      end
    end
  end

  def set_max_score(evaluation)
    company_state = siren_company_state(evaluation.value)

    if company_state == "Actif"
      evaluation.state = "favorable"
      evaluation.reason = "company_opened"
      evaluation.score = MAX_SCORE
    else
      evaluation.state = "unfavorable"
      evaluation.reason = "company_closed"
      evaluation.score = MAX_SCORE
    end
  end

  def siren_company_state(value)
    uri = URI(OPENDATASOFT_URL)
    uri.query = URI.encode_www_form({
      dataset: 'sirene_v3',
      q: value,
      sort: 'datederniertraitementetablissement',
      'refine.etablissementsiege': 'oui'
    })

    response = Net::HTTP.get(uri)
    parsed_response = JSON.parse(response)
    company_state = parsed_response["records"].first["fields"]["etatadministratifetablissement"]
  end

  def vat_company_state
    data = [
      { state: "favorable", reason: "company_opened" },
      { state: "unfavorable", reason: "company_closed" },
      { state: "unconfirmed", reason: "unable_to_reach_api" },
      { state: "unconfirmed", reason: "ongoing_database_update" },
    ].sample
  end

end

class Evaluation
  attr_accessor :type, :value, :score, :state, :reason

  def initialize(type:, value:, score:, state:, reason:)
    @type = type
    @value = value
    @score = score
    @state = state
    @reason = reason
  end

  def to_s()
    "#{@type}, #{@value}, #{@score}, #{@state}, #{@reason}"
  end
end
