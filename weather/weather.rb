require "json"
require "net/http"
require 'date'

class Weather
  META_WEATHER_URL = 'https://www.metaweather.com/api/location'
  RAIN_STATES = %w[
    Thunderstorm
    Heavy Rain
    Light Rain
    Showers
  ]

  def initialize(city)
    @city = city
  end

  def is_it_raining_tomorrow?
    woeid = city_woeid
    weather = city_weather(woeid)
    is_it_raining?(weather)
  end

  private

  def city_woeid
    body = {
      query: @city
    }

    response = meta_weather_call('search', body)
    raise InvalidCityName if response.empty?

    response.first['woeid']
  end

  def city_weather(woeid)
    tomorrow = Date.today + 1
    meta_weather_call("#{woeid}/#{tomorrow.year}/#{tomorrow.month}/#{tomorrow.day}")
  end

  def is_it_raining?(city_weather)
    rain_states = city_weather.select do |weather|
      RAIN_STATES.include?(weather["weather_state_name"])
    end

    rain_states.any?
  end

  def meta_weather_call(url, body={})
    uri = URI("#{META_WEATHER_URL}/#{url}/")
    uri.query = URI.encode_www_form(body)

    response = Net::HTTP.get(uri)
    JSON.parse(response)
  end
end

class InvalidCityName < StandardError ; end
