# Setup

ruby 3.0.0

You will need bundle in order to use this project. More on how to install bundler there : https://bundler.io/

# Description

Weather is a little command line project who call to the https://www.metaweather.com/ API and check if it's gonna rain tomorrow in a specific city.

You can find the list of the available cities on main page of there website.

# Usage

Go in the root directory of the project.

```bash
$> bundle install
$> irb
$> load './weather.rb'
$> Weather.new('Paris').is_it_raining_tomorrow?
```

# Unitary tests

This project have a few unitary tests. Make sure you have `bundle install` before so that you can access the `rspec` and `json` gems.

```bash
$> rspec 'weather_spec.rb'
```

# What would I have improve if I had more time

## Unitary tests

I would had more unitary tests so we can check every private functions. As most of the functions make use of the MetaWeather API it would have been nice to make some mock responses to test more deeply the code.

I would also take the time to refactor the ones I have done.

## Rain states

If we where in a real case I would have check which states exactly are considered as `rain` for the client.
