# frozen_string_literal: true

require File.join(File.dirname(__FILE__), "weather")

RSpec.describe Weather do
  describe "#is_it_raining_tomorrow?" do
    subject! { described_class.new(city) }

    context "when the city is valid" do
      let(:city) { 'Paris' }

      it "return a boolean" do
        expect(subject.is_it_raining_tomorrow?()).to satisfy {|value| value == true || value == false}
      end
    end

    context "when the city is invalid" do
      let(:city) { 'Toto' }

      it "return an errror" do
        expect{subject.is_it_raining_tomorrow?()}.to raise_error(InvalidCityName)
      end
    end
  end
end
